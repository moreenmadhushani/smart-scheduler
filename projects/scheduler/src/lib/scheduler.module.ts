import { NgModule } from '@angular/core';
import { SchedulerComponent } from './scheduler.component';
import { CalenderFormComponent } from './calender-form/calender-form.component';



@NgModule({
  declarations: [
    SchedulerComponent,
    CalenderFormComponent
  ],
  imports: [
  ],
  exports: [
    SchedulerComponent
  ]
})
export class SchedulerModule { }
